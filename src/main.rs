#![feature(type_ascription)]

#[macro_use]
extern crate log;

use libc;
use std::error::Error;
use std::io::Write;
use std::{ffi, io, mem, net, os::unix, thread};

use std::convert::AsRef;
use std::iter::FromIterator;
use std::os::unix::fs::FileTypeExt;
use std::os::unix::io::AsRawFd;
use std::str::FromStr;
use std::string::ToString;

use colored::Colorize;

fn run() -> io::Result<()> {
  let mut unix_file = String::from("/tmp/mysocket");
  let mut tcp_bind = String::from("0.0.0.0");
  let mut tcp_port = 23555;
  let mut argv = std::env::args().skip(1);
  loop {
    let arg = argv.next();
    if let Some(arg) = arg {
      debug!("Next arg: {:?}", arg);
      if arg.starts_with("--") {
        let mut _iter = arg.chars().skip(2);
        let flag = String::from_iter(_iter.by_ref().take_while(|x| *x != '='));
        let flag: &str = &flag;
        let flagval = String::from_iter(_iter);
        debug!("flag = {:?}, flagval = {:?}", flag, flagval);
        match flag {
          "unix-bind" => {
            if flagval.len() == 0 {
              return Err(io::Error::from(io::ErrorKind::InvalidInput));
            } else {
              unix_file = flagval;
            }
          }
          "tcp-bind" => {
            if flagval.len() == 0 {
              return Err(io::Error::from(io::ErrorKind::InvalidInput));
            } else {
              tcp_bind = flagval;
            }
          }
          _ => {
            return Err(io::Error::from(io::ErrorKind::InvalidInput));
          }
        }
      } else if arg.starts_with("-") {
        if arg.len() != 2 {
          return Err(io::Error::from(io::ErrorKind::InvalidInput));
        }
        match arg.chars().skip(1).next().unwrap() {
          'p' => {
            let nextarg = argv.next();
            if let Some(val) = nextarg {
              let port = u16::from_str(val.as_ref(): &str);
              if let Ok(port) = port {
                tcp_port = port;
              } else {
                return Err(io::Error::from(io::ErrorKind::InvalidInput));
              }
            } else {
              return Err(io::Error::from(io::ErrorKind::InvalidInput));
            }
          }
          _ => {
            return Err(io::Error::from(io::ErrorKind::InvalidInput));
          }
        }
      }
    } else {
      break;
    }
  }
  {
    match std::fs::metadata(unix_file.as_ref(): &str) {
      Err(e) => match e.kind() {
        io::ErrorKind::NotFound => {}
        _ => return Err(e),
      },
      Ok(mtd) => {
        if mtd.file_type().is_socket() {
          std::fs::remove_file(unix_file.as_ref(): &str)?;
        } else {
          error!(
            "{} already exists and is not a socket; not overwriting.",
            unix_file
          );
          return Err(io::Error::from(io::ErrorKind::AlreadyExists));
        }
      }
    }
    info!("Binding to unix:{}", &unix_file);
    info!("Binding to tcp:{}:{}", &tcp_bind, tcp_port);
  }
  let sock_unix = unix::net::UnixListener::bind(&unix_file)?;
  sock_unix.set_nonblocking(true)?;
  let sock_tcp = net::TcpListener::bind((tcp_bind.as_ref(), tcp_port))?;
  sock_tcp.set_nonblocking(true)?;
  let mut fds = [
    libc::pollfd {
      fd: sock_unix.as_raw_fd(),
      events: libc::POLLIN,
      revents: 0,
    },
    libc::pollfd {
      fd: sock_tcp.as_raw_fd(),
      events: libc::POLLIN,
      revents: 0,
    },
  ];
  loop {
    let pollret = unsafe { libc::poll(fds.as_mut_ptr(), fds.len() as _, -1) };
    if pollret >= 0 {
      match sock_tcp.accept() {
        Err(e) => match e.kind() {
          io::ErrorKind::WouldBlock => {}
          _ => {
            error!("Error accepting from tcp: {}", e.description());
          }
        },
        Ok((st, addr)) => {
          thread::spawn(move || do_tcp_ack(st, addr));
        }
      }
      match sock_unix.accept() {
        Err(e) => match e.kind() {
          io::ErrorKind::WouldBlock => {}
          _ => {
            error!("Error accepting from unix socket: {}", e.description());
          }
        },
        Ok((st, addr)) => {
          thread::spawn(move || do_unix_ack(st, addr));
        }
      }
    } else if pollret == -1 {
      return Err(io::Error::last_os_error());
    } else {
      unreachable!();
    }
  }
}

fn do_unix_ack(mut st: unix::net::UnixStream, _: unix::net::SocketAddr) -> io::Result<()> {
  st.set_nonblocking(false)?;
  let fd = st.as_raw_fd();
  let uname;
  unsafe {
    let mut ucred: libc::ucred = mem::uninitialized();
    let origsize = mem::size_of::<libc::ucred>() as u32;
    let mut newsize = origsize;
    *(libc::__errno_location()) = 0;
    if libc::getsockopt(
      fd,
      libc::SOL_SOCKET,
      libc::SO_PEERCRED,
      &mut ucred as *mut _ as *mut _,
      &mut newsize as *mut _,
    ) == -1
    {
      return Err(io::Error::last_os_error());
    }
    assert_eq!(origsize, newsize);
    *(libc::__errno_location()) = 0;
    let pwstruct = libc::getpwuid(ucred.uid);
    if pwstruct == 0 as _ {
      uname = ucred.uid.to_string().into_bytes();
    } else {
      uname = Vec::<u8>::from(ffi::CStr::from_ptr((*pwstruct).pw_name).to_bytes());
    }
  };
  info!(
    "Received connection to unix socket from user {}.",
    String::from_utf8_lossy(&uname)
  );
  st.write(b"Hello. You are ")?;
  st.write(&uname)?;
  st.write(b".\n")?;
  Ok(())
}

fn do_tcp_ack(mut st: net::TcpStream, addr: net::SocketAddr) -> io::Result<()> {
  st.set_nonblocking(false)?;
  let origaddr: String;
  unsafe {
    let mut sockaddr_buf: libc::sockaddr = mem::zeroed();
    let size = mem::size_of::<libc::sockaddr>() as u32;
    let mut newsize = size;
    if libc::getsockopt(
      st.as_raw_fd(),
      libc::SOL_IP,
      libc::SO_ORIGINAL_DST,
      &mut sockaddr_buf as *mut _ as *mut _,
      &mut newsize as *mut _,
    ) == -1
    {
      origaddr = String::from("null");
    } else {
      assert_eq!(newsize, size);
      match sockaddr_buf.sa_family as _ {
        libc::AF_INET => {
          let sockaddr: libc::sockaddr_in = mem::transmute_copy(&sockaddr_buf);
          let addr = net::Ipv4Addr::from(u32::from_be(sockaddr.sin_addr.s_addr));
          let addr = net::SocketAddrV4::new(addr, u16::from_be(sockaddr.sin_port));
          origaddr = addr.to_string();
        }
        _ => {
          origaddr = String::from("???");
        }
      }
    }
  }
  info!(
    "Received connection to tcp socket from {}, SO_ORIGINAL_DEST={}",
    addr, origaddr
  );
  st.write_fmt(format_args!(
    "Hello. Your address is {}, my address is {}, and SO_ORIGINAL_DEST={}.\n",
    addr,
    st.local_addr()?,
    origaddr
  ))?;
  Ok(())
}

struct Logger {
  stderr: io::Stderr,
}
impl log::Log for Logger {
  fn enabled(&self, _: &log::Metadata) -> bool {
    true
  }
  fn log(&self, record: &log::Record) {
    let mut stderrlock = self.stderr.lock();
    let head = format!(
      "[{}] {}:{}: ",
      record.level(),
      record.file().unwrap_or("??"),
      record
        .line()
        .map_or(String::from("??"), |n| ToString::to_string(&n))
    );
    let mut head = colored::ColoredString::from(head.as_ref());
    match record.level() {
      log::Level::Info => { head = head.blue() }
      log::Level::Error => { head = head.red() }
      log::Level::Debug => { head = head.green() }
      log::Level::Trace => { head = head.green() }
      _ => { head = head.normal() }
    }
    let _ = stderrlock.write(ToString::to_string(&head).as_bytes());
    let _ = stderrlock.write_fmt(*record.args());
    let _ = stderrlock.write(b"\n");
  }
  fn flush(&self) {
    let _ = self.stderr.lock().flush();
  }
}

fn main() {
  log::set_boxed_logger(Box::new(Logger {
    stderr: io::stderr(),
  }))
  .unwrap();
  log::set_max_level(log::LevelFilter::max());
  let res = run();
  if let Err(e) = res {
    error!("Error: {}", e.description());
    log::logger().flush();
    std::process::exit(1);
  }
}
